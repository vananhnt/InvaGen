# InvaGen
Invariants generator based on Farkas's Lemma and Craig Interpolation

- To run program with Farkas requires: Java 8 +, Python 3, Z3 (or Z3py) and Reduce (for Redlog package)

- To run program with Craig requires: MathSat

Input format:
- a +|- b =|<|>|!=|>=|<= c
a, b, c = [0..9] | [a..z]
e.g. a = -10 -> false
     a + 10 = 0 -> true
