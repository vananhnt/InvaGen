package main.craig;

import java.util.ArrayList;
import java.util.List;

import main.utils.InfixToPrefix;

public class InterpolationFormula {
	public static String LOGIC_AND = "and";
	
	public static String validateInitFormula(String pre, String con, String inv) {
		String res = "";
		String freCon = InfixToPrefix.infixToPrefix(con);
		String frePre = InfixToPrefix.infixToPrefix(pre);
		res = wrapPrefix(LOGIC_AND, freCon, frePre);
		res = wrapPrefix(LOGIC_AND, res, "(not " + inv + ")");
		return res;
	}
	
	/*
	 * inv is already in frefix form
	 */
	public static String validateInductiveFormula(String con, List<String> trans, String inv, List<String> incVars) {
		String res ="";
		String freCon = InfixToPrefix.infixToPrefix(con);
		String indInv = getInductive(inv, incVars);
		res = wrapPrefix(LOGIC_AND, concatFrefixTransition(trans), freCon);
		res = wrapPrefix(LOGIC_AND, res, inv);
		res = wrapPrefix(LOGIC_AND, res, indInv);
		System.out.println(res);
		return res;
	}
	
	/*
	 * get inductive type of formula
	 * @params: formula string and changed variables
	 * a.k.a replace x, y to x1, y1
	 */
	public static String getInductive(String formula, List<String> incVars) {
		String res = formula;
		for (int i = 0; i < incVars.size(); i++) {
			res = res.replace(incVars.get(i), incVars.get(i) + "1");
			//System.out.println(incVars.get(i));
		}
		return res;
	}
	public static String getG2Formula(String con, String post, List<String> trans) {
		String res = "";
		//System.out.println(con + " " + post);
		String freCon = InfixToPrefix.infixToPrefix(con);
		String frePost = InfixToPrefix.infixToPrefix(post);
		String freTrans = concatFrefixTransition(trans);
		if (freCon.equals("true")) res = wrapPrefix(LOGIC_AND, "(not " + freCon.trim() + ")", "(not " + frePost.trim() + ")");
		else res = "(not " + frePost.trim() + ")";
		res = wrapPrefix(LOGIC_AND, res, freTrans.trim());
		return res;
	}
	public static String getG2Formula(String con, String post) {
		String res = "";
		//System.out.println(con + " " + post);
		String freCon = InfixToPrefix.infixToPrefix(con);
		String frePost = InfixToPrefix.infixToPrefix(post);

		res = wrapPrefix(LOGIC_AND, "(not " + freCon.trim() + ")", "(not " + frePost.trim() + ")");
		return res;
	}
	public static String getG1Formula(String pre, List<String> trans) {
		String res = "";
		String frePre = InfixToPrefix.infixToPrefix(pre);
		String freTrans = concatFrefixTransition(trans);
		res = wrapPrefix(LOGIC_AND, frePre.trim(), freTrans.trim());
		return res;
	}
	public static String getG1Formula(String pre) {
		String res = "";
		String frePre = InfixToPrefix.infixToPrefix(pre);
		//String freTrans = concatFrefixTransition(trans);
		//res = wrapPrefix(LOGIC_AND, frePre.trim(), freTrans.trim());
		res = frePre.trim();
		return res;
	}
	private static String concatFrefixTransition(List<String> trans) {
		String res ="";
		List<String> formulas = new ArrayList<String>();
		trans.forEach(v -> formulas.add(InfixToPrefix.infixToPrefix(v)));
		for (int i = 0; i < formulas.size(); i++) {
			res = wrapPrefix(LOGIC_AND, res, formulas.get(i));
		}
		return res;
	}
	private static String wrapPrefix(String operand, String left, String right) {
		return "(" + operand + " " + left.trim() + " " + right.trim() + ")";
	}
}
