package main.craig;

import java.io.File;
import java.util.List;

import main.generation.ModelGeneration;
import main.utils.Resources;
import main.utils.TransitionsReader;

public class LoopSystem {
	private List<String> vars;
	private List<String> incVars; //variable that changes throughout program -- incremented variables
	private String Pre;
	private String Con;
	private String Post;
	private List<String> Transitions;
	private String fileName ="";
	private String solver = Resources.SOLVERS_MSAT;
	
	public List<String> getTransitions() {
		return Transitions;
	}
	public void setTransitions(List<String> transitions) {
		Transitions = transitions;
	}
	public LoopSystem() {
		// TODO Auto-generated constructor stub
	}
	public LoopSystem(List<String> v, List<String> inc, List<String> trans, String pre, String con, String post) {
		vars = v;
		incVars = inc;
		Transitions = trans;
		Pre = pre;
		Con = con;
		Post = post;
	}
	public LoopSystem(String filepath) {
		LoopSystem tmp = TransitionsReader.getLoopSystem(new File(filepath));
		this.setFileName((new File(filepath)).getName());
		this.Con = tmp.Con;
		this.Post = tmp.Post;
		this.Pre = tmp.Pre;
		this.vars = tmp.vars;
		this.incVars = tmp.incVars;
		this.Transitions = tmp.Transitions;
	}
	public String getG2Formula() {
		return InterpolationFormula.getG2Formula(getCon(), getPost(), getTransitions());
	}
	public String getG1Formula() {
		return InterpolationFormula.getG1Formula(getPre());
	}
	public List<String> getVars() {
		return vars;
	}
	public void setVars(List<String> vars) {
		this.vars = vars;
	}
	public String getSolver() {
		return solver;
	}
	public void setSolver(String solver) {
		this.solver = solver;
	}
	public String getPre() {
		return Pre;
	}
	public void setPre(String pre) {
		Pre = pre;
	}
	public String getCon() {
		return Con;
	}
	public void setCon(String con) {
		Con = con;
	}
	public String getPost() {
		return Post;
	}
	public void setPost(String post) {
		Post = post;
	}
	
	public String getInvariant() {
		List<String> ins = ModelGeneration.generateInterpolation(this);
		String res ="";
		for (int i = 1; i < ins.size(); i++) {
			res += ins.get(i); 
		}
		return res;
	}
	public void print() {
		vars.forEach(v -> {System.out.print(v + " ");});
		System.out.println();
		System.out.println("Pre-condition: " + Pre);
		System.out.println("Guard condition: " + Con);
		System.out.println("Post condition: " + Post);
		Transitions.forEach(v -> {System.out.println(v);});
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<String> getIncVars() {
		return incVars;
	}
	public void setIncVars(List<String> incVars) {
		this.incVars = incVars;
	}
	
	
}
