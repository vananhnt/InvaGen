package main.farkas.entity;

import java.util.List;

import main.utils.Printer;

public class Consecution extends Transition {
	
	private String[][] initializeCoefMatrix() {
		String[][] tmp = new String [row][row];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < row; j++) {
				tmp[i][j] = ((i == j && i == 0) ? "u" : ((i == j && i != 0) ? "r"+ (i - 1) : "0") );
			}
		}
		return tmp;
	}
	public void build(List<Expression> inExprs, List<String> varList) {
		this.exprs = inExprs;
		this.row = inExprs.size() + 2;
		this.col = varList.size() + 1;
		this.matrix = buildMatrix(inExprs, varList);
		this.coefMatrix = initializeCoefMatrix();
	}
	/*
	 * build matrix from a set of expressions
	 */
	private String[][] buildMatrix(List<Expression> exprs, List<String> varList) {
		int nExprs = exprs.size();//number of expressions
		int nVars = varList.size();
		String[][] matrix = new String[nExprs+2][nVars + 1];
		
		//populate first row of matrix
		//c starts at c1
		for (int j = 0; j < nVars + 1; j++) {
			matrix[0][j] = (j == nVars) ? "d" : ((j < nVars/2) ? "c" + (j + 1): "0" );
		}
		//populate second row of matrix
		for (int j = 0; j < nVars + 1; j++) {
			matrix[1][j] = (j == nVars) ? "-1" : "0"; 
		}
		//matrix block for variables
		//same as initiation matrix
		for (int i = 0; i < nExprs; i++) {
			for (int j = 0; j < nVars; j++) {
				if (exprs.get(i).getExprMap().containsKey(varList.get(j))) {
					matrix[i + 2][j] = exprs.get(i).getExprMap().get(varList.get(j));
				//if the coefficient equals 0
				} else {
					matrix[i + 2][j] = "0";
				}
			}	
		}
		//matrix block for constants
		for (int i = 0; i < nExprs; i ++) {
			matrix[i + 2][nVars] = exprs.get(i).getConstant();
		}
		return matrix;
	}
	public String[][] getFinalMatrix() {
		String[][] res = new String[row][col];
		for (int i = 0; i < row; i ++) {
			for (int j = 0; j < col; j++) {
				res[i][j] = (matrix[i][j].equals("0") ? "0" : 
							(matrix[i][j].equals("1") ? coefMatrix[i][i] :  
							(matrix[i][j].equals("-1") ? "-" + coefMatrix[i][i] : 
							matrix[i][j] + "*" + coefMatrix[i][i])));
			}
		}
		return res;
	}
	public void printFinalMatrix() {
		Printer.printMatrix(getFinalMatrix(), row, col);
	}

}
