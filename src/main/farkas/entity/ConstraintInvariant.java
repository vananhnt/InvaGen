package main.farkas.entity;

import java.util.ArrayList;
import java.util.List;

public class ConstraintInvariant {
	public List<String> coeff;
	//assume sorted coefficients' list c1, c2, c3, ... d
	//public static Map<String, String> invMap = new HashMap<>();

	public ConstraintInvariant() {
		coeff = new ArrayList<String>();
	}
	public ConstraintInvariant(String model) {
		this.coeff = getInvariantFromModel(model);
	}
	public List<String> getCoeff() {
		return coeff;
	}
	public void setCoeff(List<String> coeff) {
		this.coeff = coeff;
	}
	public String toString(List<String> vars) {
		String res ="";
		for (int i = 0; i < coeff.size(); i++) {
			if (i != coeff.size() - 1) {
				if (res.isEmpty()) {
					res += (coeff.get(i).equals("0")) ? "" : 
						(coeff.get(i).equals("1")) ? vars.get(i) :
						(coeff.get(i).equals("-1")) ? "-" + vars.get(i) :
						coeff.get(i) + vars.get(i)
						;	
				} else {
					res += (coeff.get(i).equals("0")) ? "" : 
						(coeff.get(i).equals("1")) ? "+" + vars.get(i) :
						(coeff.get(i).equals("-1")) ? "-" + vars.get(i) :
						(coeff.get(i).contains("-")) ? coeff.get(i) + vars.get(i) :
						"+" + coeff.get(i) + vars.get(i)
						;
				}
			} else {
				res += (coeff.get(i).equals("0")) ? "" : 
					(coeff.get(i).contains("-")) ? coeff.get(i) :
					"+" + coeff.get(i);
		}
		}
		if (res.isEmpty()) return null;
		return res + " <= 0";
	}
	public static List<ConstraintInvariant> getInvariantListFromModels(List<String> models) {
		List<ConstraintInvariant> invs = new ArrayList<>();
		for (String md : models) {
			invs.add(new ConstraintInvariant(md));
		}
		return invs;
	}
	private List<String> getInvariantFromModel(String model) {
		List<String> list = new ArrayList<>();
		String[] subs = model.split(",");
		for (String v : subs) {
			//System.out.println(v);
			String[] parts = v.split("=");
			if (parts.length == 2) list.add(parts[1].trim());
		}
		return list;
	}
}
