package main.farkas.entity;

import java.util.Map;

import main.utils.ExpressionParser;

public class Expression {
	private String exprStr;
	private Map<String, String> exprMap; //coefficient and variable map
	private String constant; //constants
	//chua giai quyet truong hop c*d la constant
	
	/*
	 * contructors
	 */
	public Expression() {}
	
	/*
	 * getters and setters
	 */
	
	public void setExprStr(String exprStr) {
		this.exprStr = exprStr;
	}
	
	public void setExprMap(Map<String, String> exprMap) {
		this.exprMap = exprMap;
	}
	
	public Map<String, String> getExprMap() {
		return exprMap;
	}
	
	public String getConstant() {
		return constant;
	}

	public void setConstant(String constants) {
		this.constant = constants;
	}
	
	/*
	 * printers
	 */
	public String toString() {
		return exprStr;
	}
	
	public void visit() {
		ExpressionParser.visit(exprStr);
	}
	public void print() {
		System.out.println(exprStr);
		if (exprMap != null) exprMap.forEach((k,v) -> System.out.println(k + " " + v));
		if (constant != null) System.out.println(constant);
	}
}
