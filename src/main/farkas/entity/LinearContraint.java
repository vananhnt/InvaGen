package main.farkas.entity;
@Deprecated
public class LinearContraint {
	
	static String EQ = "=";
	static String GT = ">";
	static String LT = "<";
	static String GE = ">=";
	static String LE = "<=";
	
	Expression left;
	Expression right;
	String relop; //relational operator EQ, GT, LT, GE, LE

	public Expression getLeft() {
		return left;
	}
	public void setLeft(Expression left) {
		this.left = left;
	}
	public Expression getRight() {
		return right;
	}
	public void setRight(Expression right) {
		this.right = right;
	}
	public String getRelop() {
		return relop;
	}
	public void setRelop(String relop) {
		this.relop = relop;
	}

}
