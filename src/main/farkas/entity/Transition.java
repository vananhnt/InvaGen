package main.farkas.entity;

import java.util.List;

import main.utils.Printer;

abstract class Transition {
	protected List<Expression> exprs;
	protected int row;
	protected int col;
	protected String[][] matrix;
	protected String[][] coefMatrix;
	
	/*
	 * getters and setters
	 */
	public List<Expression> getExprs() {
		return exprs;
	}
	public void setExprs(List<Expression> exprs) {
		this.exprs = exprs;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public String[][] getmatrix() {
		return matrix;
	}
	public void setmatrix(String[][] matrix, int row, int col) {
		this.matrix = matrix;
		this.col = col;
		this.row = row;
	}
	
	public String[][] getCoefMatrix() {
		return coefMatrix;
	}
	
	public void setCoefMatrix(String[][] coefMatrix, int row) {
		this.coefMatrix = coefMatrix;
		this.row = row;
	}
	
	/*
	 * printers
	 */
	public void printMatrix() {
		Printer.printMatrix(matrix, row, col);
	}
	public void printCoeffMatrix() {
		Printer.printMatrix(coefMatrix, row, row);
	}
	
}
