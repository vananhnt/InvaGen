package main.farkas.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import main.farkas.formula.RedlogFormat;
import main.farkas.formula.FormulaRefactor;
import main.generation.ModelGeneration;
import main.solver.RedlogRunner;
import main.utils.TransitionsReader;

public class TransitionSystem {
	private String fileName;
	private List<String> varList;
	private Initiation init;
	private List<Consecution> cons;
	private String preCondition;
	private String postCondition;
	private String condition;
	
	/*
	 * contructors
	 */
	public TransitionSystem() {}
	
	public TransitionSystem(List<String> v, Initiation i, List<Consecution> c) {
		this.varList = v;
		this.init = i;
		this.cons = c;
		this.fileName = "name";
	}
	public TransitionSystem(List<String> v, Initiation i, List<Consecution> c, String pre, String con, String post) {
		this.varList = v;
		this.init = i;
		this.cons = c;
		this.fileName = "name";
		this.preCondition = pre;
		this.postCondition = post;
		this.condition = con;
	}
	public TransitionSystem(String filepath) {
		TransitionSystem tmp = TransitionsReader.getTransitionSystem(new File(filepath));
		this.varList = tmp.varList;
		this.init = tmp.init;
		this.cons = tmp.cons;
		this.preCondition = tmp.preCondition;
		this.postCondition = tmp.postCondition;
		this.condition = tmp.condition;
		this.fileName = (new File(filepath)).getName();
	}
	/*
	 * functions
	 */
	public List<String> rlqe() {
		return RedlogRunner.rlqe(RedlogFormat.format(this));
	}
	public String rlqeStr() {
		return RedlogRunner.rlqeStr(RedlogFormat.format(this));
	}
	public List<String> getRedlogFormulas() {
		List<String> res = new ArrayList<String>();
		res.add(RedlogFormat.format(this.getInit()));
		this.getCons().forEach(v-> {
			res.add(RedlogFormat.format(v));
		});
		return res;
	}
	public List<String> getRedlogFormulasE() {
		List<String> res = new ArrayList<String>();
		res.add(RedlogFormat.format(this.getInit()));
		this.getCons().forEach(v-> {
			res.add(RedlogFormat.formatE(v));
		});
		return res;
	}
	public List<String> getZ3PyFormulas() {
		return FormulaRefactor.getZ3PyFormulas(this);
	}
	public String getZ3PyFormulaStr() {
		return FormulaRefactor.getZ3PyFormulaStr(this);
	}
	/* 
	 * getters and setters
	 */
	public List<String> getVarList() {
		return varList;
	}
	public void setVarList(List<String> varList) {
		this.varList = varList;
	}
	public Initiation getInit() {
		return init;
	}
	public void setInit(Initiation init) {
		this.init = init;
	}
	public List<Consecution> getCons() {
		return this.cons;
	}
	public void setCons(List<Consecution> cons) {
		this.cons = cons;
	}
	public String getFileName() {
		return fileName;
	}
	public String getPreCondition() { return preCondition; }
	public void setPreCondition(String preCondition) {
		this.preCondition = preCondition;
	}
	public String getPostCondition() {
		return postCondition;
	}
	public String getCondition() {
		return condition;
	}
	public void setPostCondition(String postCondition) {
		this.postCondition = postCondition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/*
	 * just print time here // lazy
	 */
	public List<String> getInvariants() {
		double begin = System.currentTimeMillis();
		List<String> model = ModelGeneration.generateZ3PyModel(this);
		List<ConstraintInvariant> invs = ConstraintInvariant.getInvariantListFromModels(model);
		List<String> res = new ArrayList<>();
		for (ConstraintInvariant i: invs) {
			if (i.toString(this.varList) != null) {
				res.add(i.toString(this.varList));
			}
		}
		double end = System.currentTimeMillis();
		double time = (end-begin)/1000;
		System.out.println(time);
		return res; 
	}
	/*
	 * printers
	 */
	public void print() {
		if (varList != null) varList.forEach((v) -> System.out.print(v+ " "));
		System.out.println();
		System.out.println("Initiation: ");
		if (init != null) init.printMatrix();
		System.out.println("Consecution: ");
		if (cons != null) cons.forEach((c) -> c.printMatrix());
	}
	public void printRedlogFormulas() {
		getRedlogFormulas().forEach(v -> {
			System.out.println(v);
		});
	}
	public void printModel() {
		ModelGeneration.generateZ3PyModel(this).forEach(v -> {
			System.out.println(v);
		});
	}
}
