package main.farkas.entity;

import java.util.List;

public class VariableManager {
	private List<String> list;
	
	public VariableManager() {}
	
	public VariableManager(List<String> init) {
		this.list = init;
	}
	public List<String> getVarList() {
		return list;
	}

	public void setVarList(List<String> list) {
		this.list = list;
	} 
}
