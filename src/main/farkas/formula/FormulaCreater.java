package main.farkas.formula;

import java.util.ArrayList;
import java.util.List;

import main.farkas.entity.Consecution;
import main.farkas.entity.Initiation;
import main.utils.InfixToPrefix;
/**
 * Formula Creater
 * @author va
 *
 */
public class FormulaCreater {
	public static String LOGIC_AND = "and";
	public static String LOGIC_OR = "or";
	
	/*
	 * create prefix formulas
	 */
	public static String createPrefixConsecutionFormula(Consecution cons) {
		List<String> enformulas = infixToPrefix(createInfixConsecutionEnableFormulas(cons));
		List<String> disformulas = infixToPrefix(createInfixConsecutionEnableFormulas(cons));
		String enformula = concatConsecutionFormula(enformulas);
		String disformula = concatConsecutionFormula(disformulas);
		return wrapPrefix(LOGIC_OR, enformula, disformula);
	}
	public static String createPrefixEnableConsecutionFormula(Consecution cons) {
		List<String> enformulas = infixToPrefix(createInfixConsecutionEnableFormulas(cons));
		return concatConsecutionFormula(enformulas);
	}
	public static String createPrefixDisableConsecutionFormula(Consecution cons) {
		List<String> disformulas = infixToPrefix(createInfixConsecutionDisableFormulas(cons));
		return concatConsecutionFormula(disformulas);
	}
	public static String createPrefixInitiationFormula(Initiation initiation) {
		List<String> formulas = infixToPrefix(createInfixInitiationFormulas(initiation));
		String res =  "(>= r0  0)"; //constraint r0 >= 0
		for (int i = 0; i < formulas.size(); i++) {
			res = wrapPrefix(LOGIC_AND, res, formulas.get(i));
		}
		return res;
	}
	/*
	 * create infix formulas
	 */
	public static String createInfixEnableConsecutionFormula(Consecution cons) {
		List<String> enformulas = createInfixConsecutionEnableFormulas(cons);
		return concatInfixConsecutionFormula(enformulas);
	}
	public static String createInfixDisableConsecutionFormula(Consecution cons) {
		List<String> disformulas = createInfixConsecutionDisableFormulas(cons);
		return concatInfixConsecutionFormula(disformulas);
	}
	public static String createInfixInitiationFormula(Initiation initiation) {
		List<String> formulas = createInfixInitiationFormulas(initiation);
		String res = "(r0 >= 0)";
		for (int i = 0; i < formulas.size(); i++) {
			res += " and " + "(" + formulas.get(i) + ")";
		}
		
		return res;
	}
	/*
	 * concat consecution
	 */
	private static String concatInfixConsecutionFormula(List<String> formulas) {
		String res =  "(r0 >= 0)"; //constraint r0 >= 0
		res += " and " + "(u >= 0)";
		for (int i = 0; i < formulas.size(); i++) {
			res += " and " + "(" + formulas.get(i) + ")";
		}
		return res;
	}
	private static String concatConsecutionFormula(List<String> formulas) {
		String res =  "(>= r0  0)"; //constraint r0 >= 0
		res = wrapPrefix(LOGIC_AND, res, "(>= u 0)");
		for (int i = 0; i < formulas.size(); i++) {
			res = wrapPrefix(LOGIC_AND, res, formulas.get(i));
		}
		return res;
	}
	/*
	 * main function to create infix formulas
	 */
	private static List<String> createInfixConsecutionEnableFormulas(Consecution con) {
		List<String> formulas = new ArrayList<>();
		String formula = null;
		String[][] matrix = con.getFinalMatrix();
		for (int j = 0; j < con.getCol(); j++) {
			formula = new String();
			//sum by column
			for (int i = 0; i < con.getRow(); i++) {
				formula = sumByColumn(formula, matrix[i][j]);
			}
			formulas.add(formula);
		}
		addRightHandEnableConsecution(formulas);
		return formulas;
	}
	private static List<String> createInfixConsecutionDisableFormulas(Consecution con) {
		List<String> formulas = new ArrayList<>();
		String formula = null;
		String[][] matrix = con.getFinalMatrix();
		for (int j = 0; j < con.getCol(); j++) {
			formula = new String();
			//sum by column
			for (int i = 0; i < con.getRow(); i++) {
				formula = sumByColumn(formula, matrix[i][j]);
			}
			formulas.add(formula);
		}
		addRightHandDisableConsecution(formulas);
		return formulas;
	}
	private static List<String> createInfixInitiationFormulas(Initiation initiation) {
		List<String> res = new ArrayList<String>();
		String formula = null;
		String[][] matrix = initiation.getFinalMatrix();
		for (int j = 0; j < initiation.getCol(); j++) {
			formula = new String();
			//sum by column
			for (int i = 0; i < initiation.getRow(); i++) {
				formula = sumByColumn(formula, matrix[i][j]);
			}
			res.add(formula);
		}
		addRightHandInit(res);
		return res;
	}
	/*
	 * add right hand functions
	 */
	private static void addRightHandEnableConsecution(List<String> res) {
		//res.size() = no of columns
		int n = 1;
		for (int i = 0; i < res.size() - 1; i++) {
			if (i < (res.size() - 1)/2) {
				res.set(i, res.get(i) + " = 0");
			}  else {
				res.set(i, res.get(i) + " = " + "c" + n++);
			}
		} 
		res.set(res.size()-1, res.get(res.size()-1) + " = d");
	}
	private static void addRightHandDisableConsecution(List<String> res) {
		//res.size() = no of columns
		for (int i = 0; i < res.size(); i++) {
			if (i < res.size() - 1) {
				res.set(i, res.get(i) + " = 0");
			}  else {
				res.set(i, res.get(i) + " = 1" );
			}
		} 
	}
	private static void addRightHandInit(List<String> res) {
		for (int i = 0; i < res.size(); i++) {
			if (i != res.size() - 1) {
				res.set(i, res.get(i) + " = " + "c" + (i + 1));
			} else {
				res.set(i, res.get(i) + " = " + "d");
			}
		} 
	}
	/*
	 * helpers
	 */
	private static List<String> infixToPrefix(List<String> infixStr) {
		List<String> prefixStr = new ArrayList<>();	
		infixStr.forEach((v) -> {
				prefixStr.add(InfixToPrefix.infixToPrefix(v));
			});
		return prefixStr;
	}
	private static String wrapPrefix(String operand, String left, String right) {
		return "(" + operand + " " + left.trim() + " " + right.trim() + ")";
	}
	@SuppressWarnings("unused")
	private static String wrapInfix(String operand, String left, String right) {
		return "(" + left.trim() + " " + operand + " " + right.trim() + ")";
	}
	private static String sumByColumn(String formula, String ijMatrix) {
		if (formula.length() == 0) {
			if (!ijMatrix.equals("0")) {
				formula += (ijMatrix.charAt(0) == '-') ? "(0" + ijMatrix + ")": ijMatrix;	
			}
		} else {
			if (!ijMatrix.equals("0")) {
				formula += (ijMatrix.charAt(0) == '-') ? ijMatrix: " + " + ijMatrix;	
			}  
		}
		return formula.isEmpty() ? "0" :formula;
	}
}
 