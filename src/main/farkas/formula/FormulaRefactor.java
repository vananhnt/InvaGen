package main.farkas.formula;

import java.util.ArrayList;
import java.util.List;

import main.farkas.entity.TransitionSystem;

public class FormulaRefactor {
	
	/*
	 * re-factor Z3py formulas from redlog's formula
	 */
	public static String getZ3PyFormulaStr(TransitionSystem ts) {
		return concatZ3PyFormula(getZ3PyFormulas(ts));
	}
	public static List<String> getZ3PyFormulas(TransitionSystem ts) {
		List<String> res = new ArrayList<String>();
		int nVar = ts.getVarList().size()/2;
		ts.rlqe().forEach(v -> {
			res.add(redlogToPy(v, nVar));
		});
		return res;
	}
	private static String concatZ3PyFormula(List<String> list) {
		String res = list.get(0);
		for (int i = 1; i < list.size(); i++) {
			res += "," + list.get(i);
		}
		return res;
	}
	/*
	 * format from redlog's output to input in z3 python file
	 * only re-factor one formula
	 */
	public static String redlogToPy(String s, int nVar) { 
		String res = s.replace("and", ",")
					  .replace("},,", ",")
					  .replace("(", "")
					  .replace("{", "")
					  .replace("},", ",")
					  .replace("}", "")
					  .replace(")", "")
					  .replace("<>", "!=")
					  .replace("$", "")
					  .replace(" = ", " == ")
					  .replace(",  true ,",",")
					  .replace("true", "(1 > 0)")
					  .replace(",false", "")
					  .replace("d", "c[" + (nVar) + "]");
		for (int i = 0; i <= nVar; i++) {
			res = res.replace("c" + (i + 1), "c[" + i + "]")
		   		.replace("c[" + i + "]**2", "c[" + i + "]*c[" + i + "]")
				.replace("c[" + i + "]**3", "c[" + i + "]*c[" + i + "]*c[" + i + "]");
		}
		res = splitOr(res);
		return res;
	}
	public static String prettyRedlogResult(String s) {
		String res = s.replace("and", ",")
				  .replace("},,", ",")
				  .replace("(", "")
				  .replace("{", "")
				  .replace("},", ",")
				  .replace("}", "")
				  .replace(")", "")
				  .replace("<>", "!=")
				  .replace("$", "")
				  .replace(" = ", " == ")
				  .replace(",  true ,",",");
		return res;
	}
	private static String splitOr(String s) {
		String[] splits = s.split("or");
		String res = "";
		if (splits.length > 1) {
			res = "Or(";
			String tmp = "";
			for (String v: splits) {
				String[] subSplits = v.split("and");
				tmp = "And("+ subSplits[0].trim();
				for (int i = 1; i < subSplits.length; i++) {
					tmp += "," + subSplits[i].trim(); 
				}
				tmp += ")";
				res += tmp + ", ";
			}
			res = res.substring(0, res.length() - 2);
			res += ")";	
		} else {
			res = s;
		}
		return res;
	
	}
	public static String doubleOperatorToChar(String infix) {
		infix = infix.replace("<=", "L")
				 	.replace(">=", "G")
				 	.replace("and", "&")
				 	.replace("or", "|")
				 	.replace("<>", "~")
				 	.replace("!=", "~");
		return infix;
	}
	public static String charOperatorToOriginal(String infix) {
		infix = infix.replace("L", "<=")
				 	.replace("G", ">=")
				 	.replace("&", "and")
				 	.replace("|", "or")
				 	.replace("~", "distinct")
					.replace("%", "mod")
		;

		return infix;
	}
	
}
