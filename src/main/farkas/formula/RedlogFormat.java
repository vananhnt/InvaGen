package main.farkas.formula;

import java.util.ArrayList;
import java.util.List;

import main.farkas.entity.*;

public class RedlogFormat {
	public static List<String> format(TransitionSystem ts) {
		List<String> res = new ArrayList<>();
		res.add(format(ts.getInit()));
		for (int i = 0; i < ts.getCons().size(); i ++) {
			res.add(format(ts.getCons().get(i)));
		}
		return res;
	}
	/*
	 * format initiation's formulas;
	 */
	public static String format(Initiation initiation) {
		String res = "";
		String formula = FormulaCreater.createInfixInitiationFormula(initiation);
		int nQuantifiers = initiation.getRow(); //number of quantifiers of initiation
		res = wrapEx(nQuantifiers, formula); 
		return res;
	}
	/*
	 * format consecution's formulas, split into ex(u, ex(r0, ex(r1, ...)));
	 */
	public static String formatE(Consecution consecution) {
		String en = "ex(u, " + formatEnable(consecution) + ")";
		String dis = "ex(u, " + formatDisable(consecution) + ")";
		return "(" + en + " or " + dis + ")";
	}
	/*
	 * format consecution's formulas, split into ex(u, rlqe ex(r0, r1, r2, ...));
	 */
	public static String format(Consecution consecution) {
		String res = formatEnable(consecution) + " or " + formatDisable(consecution);
		return "ex(u, rlqe " + "(" + res + "))";
		//return "(" + res + ")";
	}
	public static String formatEnable(Consecution consecution) {
		String res = "";
		String formula = FormulaCreater.createInfixEnableConsecutionFormula(consecution);
		int nQuantifiers = consecution.getRow(); //number of quantifiers of initiation
		res = wrapEx(nQuantifiers - 1, formula); 
		//res = "ex(u, " + res + ")";
		return res;
	}
	public static String formatDisable(Consecution consecution) {
		String res = "";
		String formula = FormulaCreater.createInfixDisableConsecutionFormula(consecution);
		int nQuantifiers = consecution.getRow(); //number of quantifiers of initiation
		res = wrapEx(nQuantifiers - 1, formula); 
		//res = "ex(u, " + res + ")";
		return res;
	}
	private static String wrapEx(int r, String fm) {
		if (r == 0) return fm;
		else {
			r--;
			return wrapEx(r, "ex(r" + r + ", " + fm + ")");
		}
	}
}
