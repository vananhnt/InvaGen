package main.generation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import main.craig.InterpolationFormula;
import main.craig.LoopSystem;
import main.farkas.entity.TransitionSystem;
import main.solver.SolverRunner;
import main.solver.Z3PyRunner;
import main.utils.Resources;
import main.utils.InfixToPrefix;

public class ModelGeneration {
	static String SMTINPUT_DIR = "smt/";
	
	public static List<String> generateSatResult(String formula, List<String> vars, String solver) {
		SMTInput smtInput = new SMTInput(vars, formula);
		String path = SMTINPUT_DIR + "checksat.smt";
		List<String> result = new ArrayList<String>();
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(new File(path));
			smtInput.printInputToSMT(fo);
			result = (solver.equals(Resources.SOLVERS_MSAT)) ? SolverRunner.runMathSat(path) : SolverRunner.runZ3(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public static List<String> generateZ3PyModel(TransitionSystem ts) {
		String z3formula =  ts.getZ3PyFormulaStr();//quantifiers - free formula
		SMTInput smtInput = new SMTInput(ts.getVarList(), z3formula);
		String path = SMTINPUT_DIR  + ts.getFileName().substring(0, ts.getFileName().lastIndexOf(".")) + ".py";
		List<String> result = new ArrayList<String>();
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(new File(path));
			smtInput.printInputToPython(fo);
			result = Z3PyRunner.run(path, ts.getVarList().size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public static List<String> generateZ3Model(TransitionSystem ts) {
		String rlqeFormula = ts.rlqeStr(); //quantifiers - free formula
		String z3formula = InfixToPrefix.infixToPrefix(rlqeFormula);
		SMTInput smtInput = new SMTInput(ts.getVarList(), z3formula);
		String path = SMTINPUT_DIR  + ts.getFileName().substring(0, ts.getFileName().lastIndexOf(".")) + ".smt";
		List<String> result = new ArrayList<String>();
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(new File(path));
			smtInput.printInputToFarkas(fo);
			result = SolverRunner.runZ3(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<String> generateInterpolation(LoopSystem ls) {
		String g1 = ls.getG1Formula();
		String g2 = ls.getG2Formula();
		SMTInput smtInput = new SMTInput(ls.getVars(), g1, g2);
		String pathMS = SMTINPUT_DIR  + ls.getFileName().substring(0, ls.getFileName().lastIndexOf(".")) +  "_ms.smt";
		String pathZ3 = SMTINPUT_DIR  + ls.getFileName().substring(0, ls.getFileName().lastIndexOf(".")) +  "_z3.smt";
		List<String> result = new ArrayList<String>();
		FileOutputStream fo;
		try {
			if (ls.getSolver().equals(Resources.SOLVERS_MSAT)) {
				fo = new FileOutputStream(new File(pathMS));
				smtInput.printInputToInterpolationMS(fo);
				result = SolverRunner.runMathSat(pathMS);
			} else {
				fo = new FileOutputStream(new File(pathZ3));
				smtInput.printInputToInterpolationZ3(fo);
				result = SolverRunner.runZ3(pathZ3);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

}
