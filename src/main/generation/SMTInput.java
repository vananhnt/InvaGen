package main.generation;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;


public class SMTInput {
	private List<String> variableList; //is different when using farkas or interpolant
	private String formula;
	private String g1_formula = "";
	private String g2_formula = "";
	
	public SMTInput(){
	}
	
	public SMTInput(String formula){
		this.variableList = Arrays.asList("c1", "c2", "d");
		this.formula = formula;		
	}
	public SMTInput(List<String> varList, String formula){
		this.variableList = varList;
		this.formula = formula;		
	}
	public SMTInput(List<String> varList, String g1, String g2) {
		this.variableList = varList;
		this.g1_formula = g1;
		this.g2_formula = g2;
	}
	public List<String> getVariableList() {
		return variableList;
	}
	public void setVariableList(List<String> variableList) {
		this.variableList = variableList;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	
	public void printInputToFarkas(OutputStream os) 
						throws IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(os));
		String smtType = "Int";
		/*
		 * in Farkas, the variable list include x', y' -> number of vars = number of c[x] *x
		 */
		for (int i = 0; i < variableList.size()/2; i++) {
					out.append("(declare-fun c" + (i + 1) + " () " + smtType + ")\n");
		}
		out.append("(declare-fun d () " + smtType + ")\n");
		out.append("(assert " + formula + ")\n");	
		out.append("(check-sat)\n");
		out.append("(get-model)");
		out.flush();
	    out.close();
	}
	public void printInputToSMT(OutputStream os) 
			throws IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(os));
		String smtType = "Int";
		/*
		* in Farkas, the variable list include x', y' -> number of vars = number of c[x] *x
		*/
		for (int i = 0; i < variableList.size(); i++) {
				out.append("(declare-fun " + variableList.get(i) + " () " + smtType + ")\n");
				out.append("(declare-fun " + variableList.get(i) + "1 () " + smtType + ")\n");
		}
		out.append("(declare-fun d () " + smtType + ")\n");
		out.append("(assert " + formula + ")\n");	
		out.append("(check-sat)\n");
		out.append("(get-model)");
		out.flush();
		out.close();
}
	public void printInputToInterpolationMS(OutputStream os) 
			throws IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(os));
		out.append(";; activate interpolation\n" + 
					"(set-option :produce-interpolants true)\n");
		String smtType = "Int";
		for (int i = 0; i < variableList.size(); i++) {
					out.append("(declare-const " + variableList.get(i) + " " + smtType + ")\n");
					out.append("(declare-const " + variableList.get(i) + "1 " + smtType + ")\n");
		
		}
		out.append("(define-fun A1 () Bool " + g1_formula + ")\n");
		out.append("(define-fun A2 () Bool " + g2_formula + ")\n");
		out.append("(assert (! A1 :interpolation-group g1))\n");	
		out.append("(assert (! A2 :interpolation-group g2))\n");
		out.append("(check-sat)(get-interpolant (g1))");
		out.flush();
		out.close();
	}
	
	public void printInputToInterpolationZ3(OutputStream os) 
			throws IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(os));
		out.append(";; activate interpolation\n" + 
					"(set-option :produce-interpolants true)\n");
		String smtType = "Int";
		for (int i = 0; i < variableList.size(); i++) {
					out.append("(declare-const " + variableList.get(i) + " " + smtType + ")\n");
					out.append("(declare-const " + variableList.get(i) + "1 " + smtType + ")\n");
		} 
		out.append("(compute-interpolant");
		out.append(g1_formula);
		out.append(g2_formula);
		out.append(")");
		out.flush();
		out.close();
	}
	/*
	 * for farkas 
	 */
	public void printInputToPython(OutputStream osPy) throws IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(osPy));
		out.append(
				"from z3 import *\n" +
				"import sys\n" +
				"def check(nVar):\n" + 
				"    n = nVar + 1\n" +
				"    l = [bin(x)[2:].rjust(n, '0') for x in range(2**n)]\n" + 
				"    c = list()\n" + 
				"\n" + 
				"    for x in range(0, nVar):\n" + 
				"        c.append(Int('c' + str(x + 1)))\n" + 
				"    c.append(Int('d'))\n" + 
				"    s = Solver()\n"
				);
		out.append("    s.add(" + formula + ")\n");
		out.append("    for x in range(0, len(l)):\n" + 
				"        s.push()\n" + 
				"        s.add(c[0] > 0 if l[x][0] == '1' else c[0] <= 0)\n" + 
				"        for y in range(1, n) :\n" + 
				"            s.push()\n" + 
				"            s.add(c[y] > 0 if l[x][y] == '1' else c[y] <= 0)\n" + 
				"        if (s.check() == sat): \n" + 
				"            m = s.model()\n" + 
				"            lm = list()\n" + 
				"            for d in m.decls():\n" + 
				"                lm.append(d)\n" + 
				"            lm.sort(key=getName)\n" + 
				"            for ilm in lm:\n" + 
				"                print(\"%s = %s\" % (ilm, m[ilm]), end=', ')\n" +
				"        print()\n" +
				"        for y in range (0, n):\n" + 
				"            s.pop()\n" +
				"def getName(ilm): \n" + 
				"    return ilm.name()\n"
				);
		out.append("check(int(sys.argv[1]))");
		out.flush();
	    out.close();
	}
	
	@SuppressWarnings("unused")
	private String getSMTType(String type) {
		String smtType = null;
		if (type.equalsIgnoreCase("bool"))
			smtType = "Bool";
		else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("short"))
			smtType = "Int";
		else if (type.equalsIgnoreCase("float") || type.equalsIgnoreCase("double"))
			smtType = "Real";
		
		return smtType;
	}

	@SuppressWarnings("unused")
	private String declare(String variableName, int index, String type) {
		String value = variableName + "_" + index;
		String declaration = "(declare-fun " + value + " () " + type + ")";
		return declaration;
	}

}
