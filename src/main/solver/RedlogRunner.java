package main.solver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
/*
 * Using redlog to eliminate quantifiers
 */
public class RedlogRunner {
	/*
	 * input formulas are in redlog's format
	 */
	
	public static String rlqeStr(List<String> list) {
		return concat(rlqe(list));
	}
	public static List<String> rlqe(List<String> list) {
		List<String> result = new ArrayList<String>();
		String s;	
		try {
				ProcessBuilder builder;
				if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
					builder = new ProcessBuilder("redcsl", "-w");
				} else {
					String pathToReduce = "solvers\\Reduce\\bin\\redcsl";
		    		builder = new ProcessBuilder("cmd.exe", "/c", pathToReduce, "-w");	
				}
				builder.redirectErrorStream(true);
				Process p = builder.start();
				OutputStream stdin = p.getOutputStream();
				InputStream stdout = p.getInputStream();
			
				BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

				writer.write("load_package \"redlog\"; ");
				writer.write("rlset r$; ");
				writer.write("off nat$");
				writer.write("linelength 300$");
				for (int i = 0; i < list.size(); i++) {
					try {
						writer.write((i == 0) ? "rlqe " + list.get(i) + ";" : "rlgqe " + list.get(i) + ";" );
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				writer.flush();
				writer.close();  
				while ((s = br.readLine()) != null){
					if (s.contains("=") || s.contains("<") || s.contains(">")) {
						result.add(s);
					}
					
				}
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {}
		result = regroup(result);
		return result;
	}
	public static String rlsimpl(String expr) {
		List<String> result = new ArrayList<String>();
		String s;
		try {
			ProcessBuilder builder;
			if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
				builder = new ProcessBuilder("redcsl", "-w");
			} else {
				String pathToReduce = "solvers\\Reduce\\bin\\redcsl";
				builder = new ProcessBuilder("cmd.exe", "/c", pathToReduce, "-w");
			}
			builder.redirectErrorStream(true);
			Process p = builder.start();
			OutputStream stdin = p.getOutputStream();
			InputStream stdout = p.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

			//writer.write("load_package \"redlog\"; ");
			writer.write("rlset r$; ");
			//writer.write("off nat$");
			writer.write("beta := " + expr + ";");
			writer.write("rlsimpl beta;");
			writer.flush();
			writer.close();
			while ((s = br.readLine()) != null){
				if (s.contains("=") || s.contains("<") || s.contains(">")) {
					if (!s.contains("beta"))
						result.add(s);
				}
			}
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {}
		return result.get(0);
	}
	/*
	 * remove redundant newline characters <=> regroup formulas
	 */
	private static List<String> regroup(List<String> s) {
		List<String> res = new ArrayList<>();
		for (int i = 0; i < s.size();) {
			if (!s.get(i).endsWith("$")) {
				res.add(s.get(i) + s.get(i + 1));
				i+=2;
			} else {
				res.add(s.get(i++));
			}
		}
		//System.out.println(res.size());
		return res;
	}
	@SuppressWarnings("unused")
	private static String pretty(String s) {
		return s.replace(" )", ")").replace("( ", "(")
				.replace("{", "(").replace("}", ")")
				.replace(",)", ")")
				.replace("," , " and ");
				
	}
	private static String concat(List<String> formulas) {
		String res = "(" + formulas.get(0) + ")";
		for (int i = 1; i < formulas.size();) {
				res += " and " + "(" + formulas.get(i) + ")";
			}
		return "("+ res + ")";
	}
	public static List<String> run(String cm, String str) {
		List<String> result = new ArrayList<String>();
		String s;	
		try {
				ProcessBuilder builder;
				if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
				//Process p = Runtime.getRuntime().exec("redcsl");
					builder = new ProcessBuilder("redcsl");
				} else {
					String pathToReduce = "Reduce\\bin\\redcsl";
		    		builder = new ProcessBuilder("cmd.exe", "/c", pathToReduce, "-w");
					
				}
				builder.redirectErrorStream(true);
				Process p = builder.start();
				OutputStream stdin = p.getOutputStream();
				InputStream stdout = p.getInputStream();

				BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

				writer.write("load_package \"redlog\"; ");
				writer.write("rlset r$; ");
				writer.write(cm + " " + str + ";");
				writer.flush();
				writer.close();  

				while ((s = br.readLine()) != null){
					if (s.contains("=") || s.contains("<") || s.contains(">")) {
						result.add(s);
					} 
				}
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {}
		return result;
	}
}
