package main.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/*
 * Class to run solver including mathsat and z3
 * be careful about the params of processBuilder
 */
public class SolverRunner {

	public static List<String> runMathSat(String filename) throws IOException {
		List<String> result = new ArrayList<String>();
		String s;	
		try {
				ProcessBuilder builder;
				if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
					builder = new ProcessBuilder("mathsat", "-input=smt2", filename);
				} else {
					String pathToMathsat = "solvers\\mathsat\\bin\\mathsat";
		    		builder = new ProcessBuilder("cmd.exe", "/c", pathToMathsat, "-input=smt2", filename);	
				}	
				builder.redirectErrorStream(true);
				Process p = builder.start();
				InputStream stdout = p.getInputStream();
			
				BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
				while ((s = br.readLine()) != null){
					result.add(s); 
				}
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {}
		return result;
	}
	
	public static List<String> runZ3(String filename) 
			throws IOException {
		List<String> result = new ArrayList<String>();
		String s;	
		try {
				ProcessBuilder builder;
				if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
					builder = new ProcessBuilder("z3", "-smt2", filename);
				} else {
					String pathToZ3 = "solvers\\z3\\bin\\z3";
		    		builder = new ProcessBuilder("cmd.exe", "/c", pathToZ3, "-smt2", filename);	
				}	
				builder.redirectErrorStream(true);
				Process p = builder.start();
				InputStream stdout = p.getInputStream();
			
				BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
				while ((s = br.readLine()) != null){
					result.add(s); 
				}
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {}
		return result;
	}
	
	public static List<String> runZ3(String filename, int time) 
			throws IOException {
		List<String> result = new ArrayList<String>();
		String pathToZ3 = "z3\\bin\\z3.exe";
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", pathToZ3 + " -smt2 -st -T:900" + time + filename);
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}
			result.add(line);
		}
		return result;
	}
}
