package main.solver;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Z3PyRunner {
	public static List<String> run(String filepath, int nVar) {
		List<String> result = new ArrayList<String>();
		String s;
		Integer i = new Integer(nVar/2);
		try {
				ProcessBuilder builder;
				if(System.getProperty("os.name").equalsIgnoreCase("Linux")) {
					builder = new ProcessBuilder("python3", filepath, i.toString());
				} else {
					/*
					 * haven't deal with windows yet
					 */
					String pathToReduce = "python";
		    		builder = new ProcessBuilder("cmd.exe", "/c", pathToReduce, filepath, i.toString());	
				}
				builder.redirectErrorStream(true);
				Process p = builder.start();
				InputStream stdout = p.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
				while ((s = br.readLine()) != null){
						result.add(s);
				}
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {}
		return result;
	}
}
