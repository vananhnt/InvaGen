package main.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import main.antlr4.parser.calculatorLexer;
import main.antlr4.parser.calculatorParser;
import main.antlr4.parser.calculatorParser.ExpressionContext;
import main.antlr4.parser.calculatorParser.MultiplyingExpressionContext;
import main.antlr4.parser.calculatorParser.OperatorContext;
import main.antlr4.parser.calculatorParser.PowExpressionContext;
import main.farkas.entity.Expression;

public class ExpressionParser {

	/*
	 * parse from String to Expression
	 */
	public static Expression parseExpression(String exprStr, List<String> varList) {
		Expression expr = new Expression();
		List<String> Terms = getTerms(parseToExpressionContext(exprStr));
		List<String> constants = new ArrayList<String>(); //seperate constant
		String constant = new String();	//combined constant
		
		expr.setExprMap(buildExprMap(Terms, varList));
		expr.setExprStr(exprStr);
		//expr.setConstants(Terms.subList(varList.size()*2, Terms.size()));
		
		for (int i = numberOfVarsInExpression(Terms, varList)*2; i < Terms.size(); i = i + 2) {
			//simplify: just for temporary	
			constants.add(Terms.get(i).equals("1") ? Terms.get(i + 1) : 
						 (Terms.get(i).equals("-1") ? "-" + Terms.get(i + 1) :
						  Terms.get(i) + " * " + Terms.get(i + 1)));
			
		}
		if (constants.isEmpty()) {
			constant = "0";
		} else {
			//combinding seperate constants w/ simplify
			for (int i = 0; i < constants.size(); i++) {
				constant += constants.get(i);
				if (i != constants.size()-1 && constants.get(i).contains("-")) {
					constant += " + ";
				}
			}
		}
		expr.setConstant(constant);
		return expr;
	}
	
	/**
	 * private classes
	 */
	private static int numberOfVarsInExpression(List<String> terms, List<String> varList) {
		int count = 0;
		for (int i = 1; i < terms.size(); i = i + 2) {
			if (varList.contains(terms.get(i))) {
				count++;
			}
		}
		return count;
	}
	//Tra ve map coefficients va variables 
	//To-Do: chua xu ly truong hop 0*x -> done
	private static Map<String, String> buildExprMap(List<String> listTerms, List<String> varList) {
		Map<String, String> map = new HashMap<>();
		for (int i = 0; i < listTerms.size(); i = i + 2) {
			if (varList.contains(listTerms.get(i + 1))) {
				map.put(listTerms.get(i + 1), listTerms.get(i));	
			} 
		}
		return map;
	}
	
	/* 
	 * Find Term of one-side expression
	 * Do-to: find a better word to represent both variable and coefficient
	 */
	private static List<String> getTerms(ExpressionContext exprContext) {
		List<ParseTree> mExprContext = exprContext.children; //multiply expressions
		List<ParseTree> pExprContext; // pow expression
		List<String> Terms = new ArrayList<>();
		String operator = "+";
		
		for (ParseTree iMContext : mExprContext) {
			if (iMContext instanceof OperatorContext) {
				operator = iMContext.getText();
			}
			if (iMContext instanceof MultiplyingExpressionContext ) {
				//System.out.println(iMContext.getText());
				pExprContext = ((MultiplyingExpressionContext) iMContext).children;
				if (pExprContext != null) {
					// Case1: coefficient = -1 or 1
					if (pExprContext.size() == 1 ) {
						Terms.add(operator.equals("+") ? "1" : "-1");
						Terms.add(pExprContext.get(0).getText());
					} else {
					// Case2: others
						for (ParseTree iPContext : pExprContext) {
							if (iPContext instanceof PowExpressionContext) { //get first pow expression
								//Terms.add(iPContext.getText());
								Terms.add(operator.equals("+") ? iPContext.getText() : "-" + iPContext.getText());
								operator = "+"; //reset operator
							}
						}	
					}
				} 
			}
		}
		return Terms;
	} 
	
	/*
	 * parse from String to Expression Context (just a helper)
	 */
	private static ExpressionContext parseToExpressionContext(String exprStr) {
		calculatorLexer lexer = new calculatorLexer(CharStreams.fromString(exprStr));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		calculatorParser parser = new calculatorParser(tokens);
		return parser.expression();
	}
	
	public static void visit(Expression expr) {
		visit(expr.toString());
	}
	
	public static void visit(String expr) {
		visit(parseToExpressionContext(expr), 0);
	}
	
	private static void visit(RuleContext ctx, int indentation) {
		for (int i = 0; i < indentation; i++) {
			System.out.print("  ");
		}
		System.out.print(calculatorParser.ruleNames[ctx.getRuleIndex()] + ": ");
		System.out.println(ctx.getText());
		for (int i = 0; i < ctx.getChildCount(); i ++) {
			if (ctx.getChild(i) instanceof RuleContext) {
				visit((RuleContext) ctx.getChild(i), indentation + 1);	
			} 	
		}
	}
	
	public Expression parser(RuleContext ctx) {
		Expression exp = null;
		return exp;
	}
	
} 
    

