package main.utils;

import java.util.Arrays;
import java.util.Stack;

import main.farkas.formula.FormulaRefactor;

public class InfixToPrefix {

	static Stack<Character> stack = new Stack<>();
	
	public static char operator[] = {'(', ')', '>', '<', '=', '+', '-', '*', '/' , '@', '?', '&', '|','~', 'L', 'G', '%'};
    public static char parentheses[] = {'(', ')'};
    static {
    	Arrays.sort(operator);
    }
	
	public static String infixToPrefix(String infix) {
		return  FormulaRefactor.charOperatorToOriginal(
				addParentheses(
				infixToPrefixHelper(
				FormulaRefactor.doubleOperatorToChar(infix))));
	}
	private static String addParentheses(String prefix) {
		String res = "";
		String[] elementMath = prefix.split(" ");
		String tmp = "";
		int nOps = 0;
		int length = elementMath.length;
		int iter = elementMath.length - 1;
		// count number of operators
		for (int i = 0 ; i < elementMath.length; i++) {
			if (isOperator(elementMath[i].charAt(0))) {
				nOps ++;
			}
		}
		while (nOps > 0) {
			// find most left operator
			while ((!isOperator(elementMath[iter].charAt(0)) || isParentheses(elementMath[iter].charAt(0)))
					&& iter > 0) iter --;
			//add parentheses
//			if (iter + 2 < elementMath.length) {
				tmp = "(" + elementMath[iter] + " " + elementMath[iter+1] + " " + elementMath[iter+2] + ")";	
			//}
			//copy later elements
			elementMath[iter] = tmp;
			for (int j = iter + 1; j + 2 < length; j ++) {
				elementMath[j]  = elementMath[j + 2];
			} 
			//re-evaluate variables
			length = length - 2;
			tmp = "";
			nOps --;
			iter = length - 1;
		}
		for (int i = 0; i < length; i++) {
			res = elementMath[i] + " ";
		}
		return res;
	}
	private static String infixToPrefixHelper(String infix) {
		String prefix = "";
		char ch;
		infix = reverse(infix);
		
		int length = infix.length();
		String operand = "";
		//System.out.printf("reverse: %s\n", infix);
		for (int i = 0; i < infix.length(); i++) {
			ch = infix.charAt(i);
			
			if (ch == ' ') {
				continue;
			}
			else if ( !isOperator(ch) ) {
				operand = "" + ch;
            	i++;
				while (i < length && isCharactorOfOperand(infix.charAt(i)) ) {
            		ch = infix.charAt(i);
            		operand = ch + operand;
            		i++;
            	}
				prefix += " " + operand;
				i--;
			} 
			else {
				if (ch == ')') {
					stack.push(ch);
				} 
				else if(ch == '(') {
					while (stack.peek() != ')') {
						prefix += " " + stack.pop();
					}
					
					stack.pop();
				} 
				else {
					if (stack.isEmpty()) {
						stack.push(ch);
					}
					else if (priority(stack.peek()) <= priority(ch)) {
						stack.push(ch);
					} 
					else {
						while(!stack.isEmpty() && priority(stack.peek()) >= priority(ch)) {
							prefix += " " + stack.pop();
						}
						stack.push(ch);
					}
				}
			}
		}
		while (!stack.isEmpty()) {
			prefix += " " + stack.pop();
		}

		stack.clear();
		
		String[] elementMath = prefix.split(" ");
		reverse(elementMath);
		
		prefix = "";
		
		for (String s: elementMath) {
			prefix += " " + s;
		}

		
		return prefix.trim();
	}
	
	/**
	 * reverse a string
	 * @param input
	 * @return reverse of the input
	 */
	public static String reverse(String input) 
	{
		String result = "";
		for (int i = input.length()-1; i >= 0;--i) {
			result += input.charAt(i);
		}
		
		return result;
	}
	
	
	public static int priority(char c)
	{
		if (c == '+' || c == '-') 
			return 1;
		else if ( c == '*' || c == '/' || c == '%') 
			return 2;
		else if ( c == '>' || c == '<' || c == '~' || c == '@' || c == 'L' || c == 'G') 
			return -1;
		else if ( c == '&' || c == '|') 
			return -2;
		else if ( c == ')')
			return -5;
		else 
			return 0;
	}
	
	public static boolean isOperator(char c) { // kiem tra xem co phai toan tu
		Arrays.sort(operator);
		if (Arrays.binarySearch(operator, c) > -1)
			return true;
		else 
			return false;
	}
	
	public static boolean isParentheses(char c) { // kiem tra xem co phai ngoac don
		Arrays.sort(parentheses);
		if (Arrays.binarySearch(parentheses, c) > -1)
			return true;
		else 
			return false;
	}
	
	// kiem tra la ki tu cua so hang
	public static boolean isCharactorOfOperand(char c) { 
		return c != ' ' && !isOperator(c);
	}
	
	public static void reverse(String[] arr) {
		int length = arr.length;
		int n = arr.length / 2;
		String temp;
		for (int i = 0; i < n; i++) {
			temp = arr[i];
			arr[i] = arr[length-i-1];
			arr[length-i-1] = temp;
		}
	}

	public static void main(String[] args) {
	    String infix;
	    infix = "(x = 2) and (y = 2)"; //precondition
//	    infix = "(t1 = x) and (t2 = y) and (x1 = t1 + t2) and (y = t1 + t2)"; //transition
//	    infix = "y2 < 1"; //postcondition
// 		infix =  "(t1 = x) and (t2 = y) and (x1 = t1 + t2) and (y1 = t1 + t2)";
		//infix = "(y2 < 1) and (t1 = x) and (t2 = y) and (x1 = t1 + t2) and (y1 = t1 + t2) and (t11 = x1) and (t21 = y1) and (x2 = t11 + t21) and (y2 = t11 + t21)";
		String prefix = infixToPrefix(infix);
		
		System.out.println(prefix);
				
		String[] elementMath = prefix.split(" ");
	    
//		for (String s: elementMath) {
//        	System.out.print("  " + s);
//        }
		
		reverse(elementMath);
	}

}
