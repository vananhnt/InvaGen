package main.utils;

import java.util.List;

public class Printer {
	
	public static void printMatrix(String[][] matrix, int row, int col) {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	public static void print(List<String> list) {
		list.forEach(v -> System.out.println(v));
	}
}
