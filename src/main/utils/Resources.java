package main.utils;

public final class Resources {
/*
 * solvers
 */
	public static String SOLVERS_MSAT = "msat";
	public static String SOLVERS_Z3 = "z3";	
/*
 * solvers' path
 */
	public static String MSAT_PATH_WS = "solvers\\mathsat\\bin\\mathsat";
	public static String Z3_4_3_PATH_LX = "solvers\\z3-4.3.2\\bin\\z3.exe";
	public static String Z3_4_3_PATH_WS = "solvers\\z3\\bin\\z3";
/*
 * benchmarks' path
 */
	public static String SMTINPUT_DIR = "smt/";
	public static String TASK_PATH = "tasks/";
	public static String LOOP_SV_CRAIG = TASK_PATH + "svcomp/craig/";
	public static String LOOP_SV_FARKAS = TASK_PATH + "svcomp/farkas/";
	public static String EX_FARKAS = TASK_PATH + "example/farkas/";
	public static String ASE2017_PATH = TASK_PATH + "ase2017/";
	public static String IVG_ASE_CRAIG = ASE2017_PATH + "craig/oopsla13-benchmarks-invgen/";
	public static String IVG_ASE_FARKAS = ASE2017_PATH + "farkas/oopsla13-benchmarks-invgen/";
}

