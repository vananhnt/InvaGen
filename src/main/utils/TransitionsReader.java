package main.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import main.craig.LoopSystem;
import main.farkas.entity.Consecution;
import main.farkas.entity.Expression;
import main.farkas.entity.Initiation;
import main.farkas.entity.TransitionSystem;

public class TransitionsReader {
	public static final String VARIABLES_TAG = "Variables";
	public static final String VAR_TAG = "v";
	public static final String CONST_TAG = "c";
	public static final String CONDITION_TAG = "Con";
	public static final String POST_TAG = "Post";
	public static final String PRE_TAG = "Pre";
	public static final String INITIATION_TAG = "Initiation";
	public static final String CONSTRAINT_TAG = "Constraint";
	public static final String TRANSITION_TAG = "Transition";
	/*
	 * read Loop System for Craig Interpolation
	 */
	public static LoopSystem getLoopSystem(File file) {
		LoopSystem ls = new LoopSystem();
		List<String> vars = new ArrayList<>();
		List<String> incs = new ArrayList<>();
		List<String> trans = new ArrayList<>();
		String pre = "";
		String con ="";
		String post ="";
		Document doc;
		try {
			doc = loadXMLFromFileCraig(file);
			//get variables's list
			NodeList varNodeList = doc.getElementsByTagName(VAR_TAG);
			NodeList constList = doc.getElementsByTagName(CONST_TAG);
			for (int i = 0; i < varNodeList.getLength(); i ++) {
				vars.add(varNodeList.item(i).getTextContent());
			}
			incs.addAll(vars);
			for (int i = 0; i < constList.getLength(); i++) {
				vars.add(constList.item(i).getTextContent());
			}
			ls.setVars(vars);
			//get Initiation
			trans = getCraigTransition(doc);
			pre = doc.getElementsByTagName(PRE_TAG).item(0).getTextContent().replace("&#60;", "<");
			post = doc.getElementsByTagName(POST_TAG).item(0).getTextContent().replace("&#60;", "<");
			con = doc.getElementsByTagName(CONDITION_TAG).item(0).getTextContent().replace("&#60;", "<");
		} catch (Exception e) {
			e.printStackTrace();
		}	
		ls = new LoopSystem(vars, incs, trans, pre, con, post);
		return ls;
	}
	
	private static List<String> getCraigTransition(Document doc) throws Exception {
		List<String> res = new ArrayList<>();
		NodeList consNode = doc.getElementsByTagName(TRANSITION_TAG);
		for (int i = 0; i < consNode.getLength(); i++) {
			res.add(consNode.item(i).getTextContent().replace("&#60;", "<"));
		}
		return res;
	}
	/*
	 * read Transition System for Farkas Lemma
	 */
	public static TransitionSystem getTransitionSystem(File file) {
		TransitionSystem ts = new TransitionSystem();
		List<String> vars = new ArrayList<String>();
		Initiation init = new Initiation();
		List<Consecution> cons = new ArrayList<>();
		String preCondition ="", postCondition="", condition="";
		Document doc;
		try {
			doc = loadXMLFromFileFarkas(file);
			//get variables's list
			NodeList varNodeList = doc.getElementsByTagName(VAR_TAG);
			for (int i = 0; i < varNodeList.getLength(); i ++) {
				vars.add(varNodeList.item(i).getTextContent());
			}
			ts.setVarList(vars);
			//get Initiation
			init = getInitiation(doc, vars);
			cons = getConsecutions(doc, vars);
			condition = doc.getElementsByTagName(CONDITION_TAG).item(0).getTextContent();
			preCondition = doc.getElementsByTagName(PRE_TAG).item(0).getTextContent();
			postCondition = doc.getElementsByTagName(POST_TAG).item(0).getTextContent();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		ts = new TransitionSystem(vars, init, cons, preCondition, condition, postCondition);
	
		return ts;
	}
	/*
	 * private classes
	 */
	
	/*
	 * Get initiation from input
	 */
	private static Initiation getInitiation(Document doc, List<String> varList) throws Exception {
		Initiation init = new Initiation();
		List<Expression> exprs = new ArrayList<>();
		try {
			NodeList initNodeList = doc.getElementsByTagName(INITIATION_TAG);
			NodeList initChilds = initNodeList.item(0).getChildNodes();
			//getting given constraints
			exprs = getConstraints(initChilds, varList);
			init.build(exprs, varList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return init;
	}
	/*
	 * Get given constraints
	 */
	private static List<Expression> getConstraints(NodeList nodeList, List<String> varList) {
		List<Expression> exprs = new ArrayList<>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeName().equals(CONSTRAINT_TAG)) {
				String exprStr = nodeList.item(i).getTextContent();
				if (exprStr != null) {
					exprs.add(ExpressionParser.parseExpression(exprStr, varList));
				}
			} 
		}
		return exprs;
	}
	/*
	 * Get consecution transition
	 */
	private static List<Consecution> getConsecutions(Document doc, List<String> varList) throws Exception {
		List<Expression> exprs = new ArrayList<>();
		List<Consecution> cons = new ArrayList<>();
		Consecution con = null;
		List<String> newVarList = getConsecutionVarList(varList);
		NodeList consNode = doc.getElementsByTagName(TRANSITION_TAG);
		for (int i = 0; i < consNode.getLength(); i++) {
			exprs = getConstraints(consNode.item(i).getChildNodes(), varList);
			con = new Consecution();
			con.build(exprs, newVarList);
			cons.add(con);	
		}
		return cons;
	}
	/*
	 * modify the original variable list
	 */
	private static List<String> getConsecutionVarList(List<String> original) {
		List<String> newList = new ArrayList<String>();
		for (String i : original) {
			newList.add("_" + i);
		}
		original.addAll(newList);
		return original;
	}
 	
	/*
	 * Load XML from file 
	 * Tu-san's
	 */
	private static Document loadXMLFromFileFarkas(File file) throws Exception
	{
		InputStreamReader isp = new InputStreamReader(new FileInputStream(file));
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(isp);
		
		String xml = "";
		String line = br.readLine();
		while (line != null) {
			xml += line;
			line = br.readLine();
		}
		
		xml = xml.replaceAll("&", "&amp;")
				.replace("<", "&lt;")
				.replace("&lt;?xml", "<?xml")
				.replace("&lt;TransitionSystem>", "<TransitionSystem>")
				.replace("&lt;Transitions>", "<Transitions>")
				.replace("&lt;Variables>", "<Variables>")
				.replace("&lt;v>", "<v>")
				.replace("&lt;Initiation>", "<Initiation>")
				.replace("&lt;Transition>", "<Transition>")
				.replace("&lt;Constraint>", "<Constraint>")
				.replace("&lt;Con>", "<Con>")
				.replace("&lt;Validate>", "<Validate>")
				.replace("&lt;Pre>", "<Pre>")
				.replace("&lt;Post>", "<Post>")
				.replace("&lt;/", "</")
				.replace("&#60;", "<");
		
	//	System.out.println(xml);
		
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    return builder.parse(is);
	}
	
	private static Document loadXMLFromFileCraig(File file) throws Exception
	{
		InputStreamReader isp = new InputStreamReader(new FileInputStream(file));
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(isp);
		
		String xml = "";
		String line = br.readLine();
		while (line != null) {
			xml += line;
			line = br.readLine();
		}
		
		xml = xml.replaceAll("&", "&amp;")
				.replace("<", "&lt;")
				.replace("&lt;?xml", "<?xml")
				.replace("&lt;Task>", "<Task>")
				.replace("&lt;Pre>", "<Pre>")
				.replace("&lt;Variables>", "<Variables>")
				.replace("&lt;v>", "<v>")
				.replace("&lt;c>", "<c>")
				.replace("&lt;Con>", "<Con>")
				.replace("&lt;Transition>", "<Transition>")
				.replace("&lt;Post>", "<Post>")
				.replace("&lt;/", "</");
		
	//	System.out.println(xml);
		
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    return builder.parse(is);
	}
	
	
	/*
	 * For testing purpose
	 */
	@Deprecated
	public static void readExpression(String filepath) throws IOException {
		  File file = new File(filepath);
		  BufferedReader br = new BufferedReader(new FileReader(file));
		  List<String> varList = Arrays.asList("i", "j");
		  String st;
		  Expression expr = null;
		while ((st = br.readLine()) != null) {
			expr = ExpressionParser.parseExpression(st, varList);
			expr.print();
		  } 
			br.close();
	}
	public static void main(String[] args) {
		TransitionSystem ts = new TransitionSystem("./problems/input.xml");
		ts.getCons().forEach((v) -> v.printCoeffMatrix());
		ts.getInit().printCoeffMatrix();
	}
}

