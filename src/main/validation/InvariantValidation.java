package main.validation;

import com.sun.org.apache.xpath.internal.operations.Bool;
import main.craig.InterpolationFormula;
import main.craig.LoopSystem;
import main.farkas.entity.TransitionSystem;
import main.generation.ModelGeneration;
import main.solver.RedlogRunner;
import main.utils.InfixToPrefix;
import main.utils.Resources;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

public class InvariantValidation {
    public static String LOGIC_AND = "and";
    @Deprecated
    public static List<String> validateInvariant(LoopSystem ls) {
        List<String> res = new ArrayList<>();
        String initFormula = InterpolationFormula.validateInitFormula(ls.getPre(), ls.getCon(), ls.getInvariant());
        String inductiveFormula = InterpolationFormula.validateInductiveFormula(ls.getCon(), ls.getTransitions(), ls.getInvariant(), ls.getIncVars());
        res.addAll(ModelGeneration.generateSatResult(initFormula, ls.getVars(), ls.getSolver()));
        res.addAll(ModelGeneration.generateSatResult(inductiveFormula, ls.getVars(), ls.getSolver()));
        return res;
    }
    /*
        filter out some unwanted invariants for Farkas
     */
    //TODO: isValid is turned off
    public static boolean validateInvariant(TransitionSystem ts, String inv) {
        boolean isValid = true;
        boolean isFilter = true;
        String freInv = InfixToPrefix.infixToPrefix(RedlogRunner.rlsimpl(inv));
        //System.out.println(RedlogRunner.rlsimpl(inv));
        String initFormula = initValidationFormula(ts.getPreCondition(), freInv);
        String postFormula = postValidationFormula(ts.getPostCondition(), ts.getCondition(), freInv);
        String satInit = ModelGeneration.generateSatResult(initFormula, ts.getVarList().subList(0, ts.getVarList().size()/2), Resources.SOLVERS_Z3).get(0);
        String satPost = ModelGeneration.generateSatResult(postFormula, ts.getVarList().subList(0, ts.getVarList().size()/2), Resources.SOLVERS_Z3).get(0);
        if (satInit.equals("sat") || satPost.equals("sat")) {
            isValid = false;
        }
        isFilter = filterInvWithPost(inv, ts.getPostCondition(), ts.getVarList().subList(0, ts.getVarList().size()/2));
        return isFilter;
    }
    public static boolean filterInvWithPost(String inv, String post, List<String> varList) {
        if (inv.length() <= 15) return true;
        for (int i = 0; i < varList.size(); i++) {
            if (!post.contains(varList.get(i)) & (inv.contains(varList.get(i)))) {
                return false;
            }
        }
        return true;
    }
    public static String initValidationFormula(String pre, String inv) {
        String res = "";
        String frePre = InfixToPrefix.infixToPrefix(pre);
        res = wrapPrefix(LOGIC_AND, frePre, "(not " + inv +")");
        return res;
    }

    public static String postValidationFormula(String post, String con, String inv) {
        String res = "";
        String freCon = InfixToPrefix.infixToPrefix(con);
        String frePost = InfixToPrefix.infixToPrefix(post);
        res = wrapPrefix(LOGIC_AND, inv, "(not " + con + ")");
        res = wrapPrefix("=>", res, post);
        return res;
    }
    /*
     * get inductive type of formula
     * @params: formula string and changed variables
     * a.k.a replace x, y to x1, y1
     */
    public static String getInductive(String formula, List<String> incVars) {
        String res = formula;
        for (int i = 0; i < incVars.size(); i++) {
            res = res.replace(incVars.get(i), incVars.get(i) + "1");
            //System.out.println(incVars.get(i));
        }
        return res;
    }
    private static String wrapPrefix(String operand, String left, String right) {
        return "(" + operand + " " + left.trim() + " " + right.trim() + ")";
    }
}
