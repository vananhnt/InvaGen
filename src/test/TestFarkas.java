package test;

import main.farkas.entity.TransitionSystem;
import main.solver.RedlogRunner;
import main.utils.Resources;
import main.validation.InvariantValidation;

public class TestFarkas {

	public static void main(String[] args) {
		TransitionSystem ts = new TransitionSystem(Resources.IVG_ASE_FARKAS + "01.xml");
		ts.getInvariants().forEach(v -> {
			if(InvariantValidation.validateInvariant(ts, v)) {
				System.out.println(RedlogRunner.rlsimpl(v));
			}
		});

	}
}
