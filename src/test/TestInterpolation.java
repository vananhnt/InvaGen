package test;

import main.craig.LoopSystem;
import main.generation.ModelGeneration;
import main.utils.Resources;

public class TestInterpolation {
	static String FILE = Resources.IVG_ASE_CRAIG + "23.xml";
	//static String FILE = Conf.LOOP_SV_CRAIG + "fib.xml";
	public static void main(String[] args) {
		LoopSystem ls = new LoopSystem(FILE);
		//System.out.println(ModelGeneration.generateInterpolation(ls));
		ls.setSolver(Resources.SOLVERS_MSAT);
		ModelGeneration.generateInterpolation(ls).forEach(v -> {
		    System.out.println(v);
		}
		);
	}
}
