package test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.antlr.v4.runtime.RuleContext;

import main.antlr4.parser.calculatorParser;

public class TestParser {
	static List<String> variables = Arrays.asList("x1", "x2");
	static String init = "(c1*d)*x1-2*x2 + d =0";

	static void visit(RuleContext ctx, int indentation) {
		for (int i = 0; i < indentation; i++) {
			System.out.print("  ");
		}
		System.out.print(calculatorParser.ruleNames[ctx.getRuleIndex()] + ": ");
		System.out.println(ctx.getText());
		for (int i = 0; i < ctx.getChildCount(); i ++) {
			if (ctx.getChild(i) instanceof RuleContext) {
				visit((RuleContext) ctx.getChild(i), indentation + 1);	
			} 	
		}
	}
	
	public static void main(String[] args) throws Exception {
		FileInputStream finStream = new FileInputStream(new File("./config.txt"));
		Properties props = new Properties();
		props.load(finStream);
		finStream.close();
	
		
	}
	
}
