;; activate interpolation
(set-option :produce-interpolants true)

(declare-const x Int)
(declare-const y Int)
(declare-const x1 Int)
(declare-const y1 Int)
(declare-const n Int)

(define-fun A1 () Bool (and (and (and (and (= x n) (= y 0)) (>= n 0)) (= x1 (- x 1))) (= y1 (+ y 1)))  )
(define-fun A2 () Bool (and (not (= y n)) (<= x 0)) )

(assert (! A1 :interpolation-group g1))
(assert (! A2 :interpolation-group g2))

(check-sat)(get-interpolant (g1))
