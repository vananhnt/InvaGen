from z3 import *

def get_n_solutions(solver, N):
  solver.push()
  solutions = []
  while len(solutions) < N:
    if solver.check() != sat:
      break
    m = solver.model()
    solutions.append(m)
    sol = []
    for v in m:
      x = v()
      sol.append(x == m[x])
    solver.add(Not(And(sol)))
  solver.pop()
  return solutions

def check(nVar) :
  c = list()
  for x in range(0, nVar - 1):
    c.append(Int('c' + str(x + 1)))
  c.append(Int('d'))
  print(c)
  s = Solver()
  s.add(2*c[0] + c[2] <= 0, c[1] != 0, c[0] <= 0, 2*c[0] + c[1] <= 0)
  for i in range(0, 5) :
    print(get_n_solutions(s, 5)[i])

#  for i in range(0, nVar):
#    if s.check() == sat :
#      print(s.model())
#    s.add(c[i] == -1)
  
#  for i in range(0, nVar):
#    if s.check() == sat :
#      print(s.model())
#    s.add(c[i] == 0)
    
#  for i in range(0, nVar):
#    if s.check() == sat :
#      print(s.model())
#    s.add(c[i] == 1)
  
  #for j in range(0, nVar):
  #s.add(c[0] != s.model()[c[0]]) 
  
  #while s.check() == sat:
  #  print(s.model())
  #  s.add(Or(c1 != s.model()[c1], c2 != s.model()[c2])) 

check(3)