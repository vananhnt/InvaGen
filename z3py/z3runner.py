from z3 import *

def getName(ilm): 
    return ilm.name()

def check(nVar):
    n = nVar + 1
    l = [bin(x)[2:].rjust(n, '0') for x in range(2**n)]
    c = list()

    for x in range(0, nVar):
        c.append(Int('c' + str(x + 1)))
    c.append(Int('d'))
    s = Solver()
    
    s.add(c[2] <= 0, c[1] != 0, c[0] != 0, c[0] - c[1] == 0, 2*c[0] - c[1] <= 0, c[0] + 2*c[1] <= 0)
    #s.add(c[2] <= 0,c[1] != 0,c[0] != 0,c[0] - c[1] == 0 ,  2*c[0] - c[1] <= 0,c[1] != 0,c[0] + 2*c[1] <= 0)

    for x in range(0, len(l)):
        s.push()
        s.add(c[0] > 0 if l[x][0] == '1' else c[0] <= 0)
        for y in range(1, n):
            s.push()
            s.add(c[y] > 0 if l[x][y] == '1' else c[y] <= 0)
        if (s.check() == sat): 
            #print(s.model())
            m = s.model()
            #print("traversing model...")
            lm = list()
            for d in m.decls():
                lm.append(d)
            lm.sort(key=getName)
            
            for ilm in lm:
                print("%s = %s" % (ilm, m[ilm]))
        for y in range (0, n):
            s.pop()

check(2)